DROP TABLE IF EXISTS groups;

CREATE TABLE groups (
    group_id serial,
    group_name character varying (50) NOT NULL,
    PRIMARY KEY (group_id)
);

DROP TABLE IF EXISTS students CASCADE;

CREATE TABLE students(
    student_id serial,
    group_id INT,
    first_name character varying(50) NOT NULL,
    last_name character varying(50) NOT NULL,
    PRIMARY KEY (student_id)
);

DROP TABLE IF EXISTS courses CASCADE;

CREATE TABLE courses(
    course_id serial,
    course_name character varying(50) NOT NULL,
    course_description text,
    PRIMARY KEY (course_id)
);

DROP TABLE IF EXISTS students_courses;

CREATE TABLE students_courses(
    student_id INT NOT NULL,
    course_id INT NOT NULL,
    FOREIGN KEY (student_id) REFERENCES students (student_id) ON DELETE CASCADE,
    FOREIGN KEY (course_id) REFERENCES courses (course_id) ON DELETE CASCADE,
    PRIMARY KEY (student_id, course_id)
);