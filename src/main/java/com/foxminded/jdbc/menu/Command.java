package com.foxminded.jdbc.menu;

public enum Command {

    FIND_GROUPS_BY_LENGTH,
    FIND_STUDENTS_BY_COURSE,
    ADD_STUDENT,
    DELETE_STUDENT,
    ADD_STUDENT_TO_COURSE,
    REMOVE_STUDENT_FROM_COURSE,
    EXIT
}
