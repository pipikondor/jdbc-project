package com.foxminded.jdbc.menu;

import com.foxminded.jdbc.logic.CourseLogic;
import com.foxminded.jdbc.logic.GroupLogic;
import com.foxminded.jdbc.logic.ResourceLoader;
import com.foxminded.jdbc.logic.StudentLogic;

import java.util.Scanner;
import java.util.stream.Collectors;

public class Menu {

    private static final String MESSAGE_ENTER_COURSE_NAME = "Enter course name: ";
    private static final String MESSAGE_ENTER_STUDENT_NAME = "Enter student name: ";
    private static final String MESSAGE_ENTER_STUDENT_LASTNAME = "Enter student last name: ";
    private static final String MESSAGE_ENTER_GROUP_ID = "Enter group id: ";
    private static final String MESSAGE_ENTER_STUDENT_ID = "Enter student id: ";
    private static final String MESSAGE_ENTER_COURSE_ID = "Enter course id: ";
    private static final String MESSAGE_ENTER_GROUP_SIZE = "Enter group size: ";
    private static final String MESSAGE_ENTER_COMMAND = "Enter one of commands to do something here:";
    private final CourseLogic courseLogic;
    private final StudentLogic studentLogic;
    private final GroupLogic groupLogic;
    private final ResourceLoader resourceLoader;

    public Menu(CourseLogic courseLogic, StudentLogic studentLogic, GroupLogic groupLogic, ResourceLoader resourceLoader) {
        this.courseLogic = courseLogic;
        this.studentLogic = studentLogic;
        this.groupLogic = groupLogic;
        this.resourceLoader = resourceLoader;
    }

    public void runApp() {
        Command command;
        Scanner scanner = new Scanner(System.in);
        System.out.println(showMenu());
        while (scanner.hasNextLine()) {
            command = getCommand(scanner.nextLine());
            if (command == Command.EXIT) {
                return;
            }
            System.out.println(doCommand(command));
            System.out.println(showMenu());
        }
    }

    private String showMenu() {
        return resourceLoader.getFileAsStream("menuText").collect(Collectors.joining(System.lineSeparator())) +
            System.lineSeparator() + MESSAGE_ENTER_COMMAND;
    }

    private Command getCommand(String commandAsString) {
        if (commandAsString == null) {
            throw new IllegalArgumentException();
        }
        Command command;
        try {
            command = Command.valueOf(commandAsString);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException();
        }
        return command;
    }

    private String doCommand(Command command) {
        if (command == Command.FIND_GROUPS_BY_LENGTH) {
            return groupLogic.findGroupsByLength(getInputInt(MESSAGE_ENTER_GROUP_SIZE)).toString();
        }
        if (command == Command.FIND_STUDENTS_BY_COURSE) {
            return courseLogic.findStudentsByCourse(getInputString(MESSAGE_ENTER_COURSE_NAME)).toString();
        }
        if (command == Command.ADD_STUDENT) {
            String name = getInputString(MESSAGE_ENTER_STUDENT_NAME);
            String lastName = getInputString(MESSAGE_ENTER_STUDENT_LASTNAME);
            int groupId = getInputInt(MESSAGE_ENTER_GROUP_ID);
            return String.valueOf(studentLogic.addStudent(name, lastName, groupId));
        }
        if (command == Command.DELETE_STUDENT) {
            return String.valueOf(studentLogic.deleteStudent(getInputInt(MESSAGE_ENTER_STUDENT_ID)));
        }
        if (command == Command.ADD_STUDENT_TO_COURSE) {
            int studentId = getInputInt(MESSAGE_ENTER_STUDENT_ID);
            int courseId = getInputInt(MESSAGE_ENTER_COURSE_ID);
            return String.valueOf(studentLogic.addStudentToCourse(studentId, courseId));
        }
        if (command == Command.REMOVE_STUDENT_FROM_COURSE) {
            int studentId = getInputInt(MESSAGE_ENTER_STUDENT_ID);
            int courseId = getInputInt(MESSAGE_ENTER_COURSE_ID);
            return String.valueOf(studentLogic.removeStudentFromCourse(studentId, courseId));
        }
        throw new IllegalArgumentException();
    }

    private String getInputString(String message) {
        Scanner scanner = new Scanner(System.in);
        String inputString = "";
        System.out.println(message);
        if (scanner.hasNextLine()) {
            inputString = scanner.nextLine();
        }
        return inputString;
    }

    private int getInputInt(String message) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(message);
        int inputInt = 0;
        if (scanner.hasNextLine()) {
            inputInt = scanner.nextInt();
        }
        return inputInt;
    }
}
