package com.foxminded.jdbc.dao;

import com.foxminded.jdbc.logic.ResourceLoader;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connect {

    private static Connect instance;
    private Connection connection;

    private Connect() {
        ResourceLoader resourceLoader = new ResourceLoader();
        String url = resourceLoader.getPropertyValue("db.url");
        String login = resourceLoader.getPropertyValue("db.login");
        String password = resourceLoader.getPropertyValue("db.password");
        try {
            Class.forName(resourceLoader.getPropertyValue("db.Driver"));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            connection = DriverManager.getConnection(url, login, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static Connect getInstance() throws SQLException {
        if (instance == null || instance.getConnection().isClosed()) {
            instance = new Connect();
        }
        return instance;
    }

    public Connection getConnection() {
        return connection;
    }
}
