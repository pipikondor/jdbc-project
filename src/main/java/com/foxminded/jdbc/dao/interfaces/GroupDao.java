package com.foxminded.jdbc.dao.interfaces;

import com.foxminded.jdbc.model.Group;

import java.util.List;

public interface GroupDao extends ModelDao<Group> {

    List<Group> findAllLessSize(int studentCount);
}
