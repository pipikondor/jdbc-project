package com.foxminded.jdbc.dao.interfaces;

import com.foxminded.jdbc.model.Student;

public interface StudentDao extends ModelDao<Student> {

    boolean addToCourse(int studentId, int courseId);

    boolean deleteById(int id);

    boolean deleteFromCourse(int studentId, int courseId);
}
