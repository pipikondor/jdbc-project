package com.foxminded.jdbc.dao.interfaces;

import com.foxminded.jdbc.model.Course;
import com.foxminded.jdbc.model.Student;

import java.util.List;

public interface CourseDao extends ModelDao<Course> {

    List<Student> findStudents(String courseName);
}
