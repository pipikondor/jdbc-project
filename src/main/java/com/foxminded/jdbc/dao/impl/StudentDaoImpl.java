package com.foxminded.jdbc.dao.impl;

import com.foxminded.jdbc.dao.interfaces.StudentDao;
import com.foxminded.jdbc.model.Student;
import com.foxminded.jdbc.dao.Connect;

import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class StudentDaoImpl implements StudentDao {

    private static final String SELECT_ALL_STUDENTS = "SELECT * FROM students";
    private static final String ADD_TO_COURSE =
        "INSERT INTO students_courses(student_id, course_id) VALUES(?, ?)";
    private static final String ADD_STUDENT =
        "INSERT INTO students(group_id, first_name, last_name) VALUES(?, ?, ?)";
    private static final String DELETE_BY_ID = "DELETE FROM students WHERE student_id = ?";
    private static final String REMOVE_FROM_COURSE =
        "DELETE FROM students_courses WHERE student_id = ? AND course_id = ?";

    @Override
    public List<Student> findAll() {
        List<Student> result = new ArrayList<>();
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            Connection connection = Connect.getInstance().getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(SELECT_ALL_STUDENTS);
            while (resultSet.next()) {
                String name = resultSet.getString(3);
                String lastName = resultSet.getString(4);
                Student student = new Student(name, lastName);
                student.setGroupId(resultSet.getInt(2));
                student.setId(resultSet.getInt(1));
                result.add(student);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return result;
        } finally {
            close(resultSet);
            close(statement);
        }
        return result;
    }

    @Override
    public boolean addToCourse(int studentId, int courseId) {
        PreparedStatement statement = null;
        try {
            Connection connection = Connect.getInstance().getConnection();
            statement = connection.prepareStatement(ADD_TO_COURSE);
            statement.setInt(1, studentId);
            statement.setInt(2, courseId);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            close(statement);
        }
        return true;
    }

    @Override
    public boolean insert(Student student) {
        if (student == null) {
            return false;
        }
        PreparedStatement statement = null;
        try {
            Connection connection = Connect.getInstance().getConnection();
            statement = connection.prepareStatement(ADD_STUDENT);
            if (student.getGroupId() == 0) {
                statement.setNull(1, 0);
            } else {
                statement.setInt(1, student.getGroupId());
            }
            statement.setString(2, student.getName());
            statement.setString(3, student.getLastName());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            close(statement);
        }
        return true;
    }

    @Override
    public boolean deleteById(int id) {
        PreparedStatement statement = null;
        try {
            Connection connection = Connect.getInstance().getConnection();
            statement = connection.prepareStatement(DELETE_BY_ID);
            statement.setInt(1, id);
            if (statement.executeUpdate() == 0) {
                return false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            close(statement);
        }
        return true;
    }

    @Override
    public boolean deleteFromCourse(int studentId, int courseId) {
        PreparedStatement statement = null;
        try {
            Connection connection = Connect.getInstance().getConnection();
            statement = connection.prepareStatement(REMOVE_FROM_COURSE);
            statement.setInt(1, studentId);
            statement.setInt(2, courseId);
            if (statement.executeUpdate() == 0) {
                return false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            close(statement);
        }
        return true;
    }
}
