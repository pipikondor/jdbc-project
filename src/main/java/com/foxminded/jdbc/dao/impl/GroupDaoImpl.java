package com.foxminded.jdbc.dao.impl;

import com.foxminded.jdbc.dao.interfaces.GroupDao;
import com.foxminded.jdbc.model.Group;
import com.foxminded.jdbc.dao.Connect;

import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class GroupDaoImpl implements GroupDao {

    private static final String SELECT_GROUPS_LESS_OR_EQUALS_SIZE = "SELECT * FROM groups WHERE group_id NOT IN " +
        "(SELECT group_id FROM students GROUP BY group_id HAVING COUNT(group_id) > ?)";
    private static final String ADD_GROUP = "INSERT INTO groups(group_name) VALUES(?)";
    private static final String SELECT_ALL_GROUPS = "SELECT * FROM groups";

    @Override
    public boolean insert(Group group) {
        if (group == null) {
            return false;
        }
        PreparedStatement statement = null;
        try {
            Connection connection = Connect.getInstance().getConnection();
            statement = connection.prepareStatement(ADD_GROUP);
            statement.setString(1, group.getName());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            close(statement);
        }
        return true;
    }

    @Override
    public List<Group> findAll() {
        List<Group> result = new ArrayList<>();
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            Connection connection = Connect.getInstance().getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(SELECT_ALL_GROUPS);
            while (resultSet.next()) {
                Group group = new Group(resultSet.getString(2));
                group.setId(resultSet.getInt(1));
                result.add(group);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return result;
        } finally {
            close(resultSet);
            close(statement);
        }
        return result;
    }

    @Override
    public List<Group> findAllLessSize(int studentCount) {
        List<Group> result = new ArrayList<>();
        if (studentCount < 0) {
            return result;
        }
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            Connection connection = Connect.getInstance().getConnection();
            statement = connection.prepareStatement(SELECT_GROUPS_LESS_OR_EQUALS_SIZE);
            statement.setInt(1, studentCount);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Group group = new Group(resultSet.getString(2));
                group.setId(resultSet.getInt(1));
                result.add(group);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return result;
        } finally {
            close(resultSet);
            close(statement);
        }
        return result;
    }
}
