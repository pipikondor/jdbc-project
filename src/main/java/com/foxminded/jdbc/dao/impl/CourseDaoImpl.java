package com.foxminded.jdbc.dao.impl;

import com.foxminded.jdbc.dao.interfaces.CourseDao;
import com.foxminded.jdbc.model.Course;
import com.foxminded.jdbc.model.Student;
import com.foxminded.jdbc.dao.Connect;

import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class CourseDaoImpl implements CourseDao {

    private static final String ADD_COURSE = "INSERT INTO courses(course_name) VALUES(?)";
    private static final String GET_STUDENTS_BY_COURSE =
        "SELECT * FROM students WHERE student_id IN " +
            "(SELECT student_id FROM students_courses WHERE course_id = " +
            "(SELECT course_id FROM courses WHERE course_name = ?))";
    private static final String GET_ALL_COURSES = "SELECT * FROM courses";

    @Override
    public boolean insert(Course course) {
        if (course == null) {
            return false;
        }
        PreparedStatement statement = null;
        try {
            Connection connection = Connect.getInstance().getConnection();
            statement = connection.prepareStatement(ADD_COURSE);
            statement.setString(1, course.getName());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            close(statement);
        }
        return true;
    }

    @Override
    public List<Student> findStudents(String courseName) {
        List<Student> result = new ArrayList<>();
        if (courseName == null) {
            return result;
        }
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            Connection connection = Connect.getInstance().getConnection();
            statement = connection.prepareStatement(GET_STUDENTS_BY_COURSE);
            statement.setString(1, courseName);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Student student = new Student(resultSet.getString(3), resultSet.getString(4));
                student.setId(resultSet.getInt(1));
                student.setGroupId(resultSet.getInt(2));
                result.add(student);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return result;
        } finally {
            close(resultSet);
            close(statement);
        }
        return result;
    }

    @Override
    public List<Course> findAll() {
        List<Course> courses = new ArrayList<>();
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            Connection connection = Connect.getInstance().getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(GET_ALL_COURSES);
            while (resultSet.next()) {
                Course course = new Course(resultSet.getString(2));
                course.setId(resultSet.getInt(1));
                courses.add(course);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return courses;
        } finally {
            close(resultSet);
            close(statement);
        }
        return courses;
    }
}
