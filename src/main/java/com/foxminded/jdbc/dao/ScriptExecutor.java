package com.foxminded.jdbc.dao;

import org.apache.ibatis.jdbc.ScriptRunner;

import java.io.Reader;
import java.io.StringReader;
import java.sql.SQLException;

public class ScriptExecutor {

    public boolean createEmptyTables(String sqlScript) {
        Reader stringReader = new StringReader(sqlScript);
        try {
            ScriptRunner scriptRunner = new ScriptRunner(Connect.getInstance().getConnection());
            scriptRunner.setAutoCommit(true);
            scriptRunner.setLogWriter(null);
            scriptRunner.runScript(stringReader);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
