package com.foxminded.jdbc.model;

import com.foxminded.jdbc.model.interfaces.Model;

import java.util.Objects;

public class Course implements Model {

    private final String name;
    private int id;


    public Course(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (o == null || o.getClass() != getClass()) {
            return false;
        }
        Course course = (Course) o;
        return name.equals(course.name) && id == course.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, id);
    }
}
