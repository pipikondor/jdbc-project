package com.foxminded.jdbc.model;

import com.foxminded.jdbc.model.interfaces.Model;

import java.util.Arrays;
import java.util.Objects;

public class Student implements Model {

    private final String name;
    private final String lastName;
    private int id;
    private int groupId;
    private int[] courses;

    public Student(String name, String lastName) {
        this.name = name;
        this.lastName = lastName;
    }

    public int[] getCourses() {
        return courses;
    }

    public void setCourses(int[] courses) {
        this.courses = courses;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public int getGroupId() {
        return groupId;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Student student = (Student) o;
        return groupId == student.groupId && Objects.equals(name, student.name)
            && Objects.equals(lastName, student.lastName) && Arrays.equals(courses, student.courses);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name, lastName, groupId);
        result = 31 * result + Arrays.hashCode(courses);
        return result;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + lastName + " " + groupId;
    }
}
