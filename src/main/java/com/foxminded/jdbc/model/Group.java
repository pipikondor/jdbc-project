package com.foxminded.jdbc.model;

import com.foxminded.jdbc.model.interfaces.Model;

import java.util.Objects;

public class Group implements Model {

    private final String name;
    private int id;
    private int studentsCount;

    public Group(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setStudentsCount(int studentsCount) {
        this.studentsCount = studentsCount;
    }

    public int getStudentsCount() {
        return studentsCount;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Group group = (Group) o;
        return name.equals(group.name) && id == group.id && studentsCount == group.studentsCount;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, id, studentsCount);
    }

    @Override
    public String toString() {
        return "group_id: " + id + " group_name: " + name;
    }
}
