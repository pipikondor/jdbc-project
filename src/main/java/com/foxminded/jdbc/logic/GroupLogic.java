package com.foxminded.jdbc.logic;

import com.foxminded.jdbc.dao.interfaces.GroupDao;
import com.foxminded.jdbc.model.Group;

import java.util.ArrayList;
import java.util.List;

public class GroupLogic {

    private final GroupDao groupDao;

    public GroupLogic(GroupDao groupDao) {
        this.groupDao = groupDao;
    }

    public List<Group> findGroupsByLength(int length) {
        if (length < 0) {
            return new ArrayList<>();
        }
        return groupDao.findAllLessSize(length);
    }
}
