package com.foxminded.jdbc.logic;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.stream.Stream;

public class ResourceLoader {

    public Stream<String> getFileAsStream(String propertyName) {
        Stream<String> fileAsStream = Stream.of("");
        String filename = getPropertyValue(propertyName);
        URL filePath = getClass().getClassLoader().getResource(filename);
        if (filePath == null) {
            return fileAsStream;
        }
        try {
            fileAsStream = Files.lines(Paths.get(filePath.toURI()));
        } catch (IOException | URISyntaxException e) {
            return fileAsStream;
        }
        return fileAsStream;
    }

    public String getPropertyValue(String propertyName) {
        String propertyValue = "";
        if (propertyName == null) {
            return propertyValue;
        }
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("config.properties");
        Properties config = new Properties();
        try {
            config.load(inputStream);
            propertyValue = config.getProperty(propertyName);
            if (propertyValue == null) {
                propertyValue = "";
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return propertyValue;
    }
}
