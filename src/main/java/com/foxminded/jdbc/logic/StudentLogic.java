package com.foxminded.jdbc.logic;

import com.foxminded.jdbc.dao.interfaces.StudentDao;
import com.foxminded.jdbc.model.Student;

public class StudentLogic {

    private final StudentDao studentDao;

    public StudentLogic(StudentDao studentDao) {
        this.studentDao = studentDao;
    }

    public boolean addStudent(String name, String lastName, int groupId) {
        if (name == null || lastName == null || groupId < 0) {
            return false;
        }
        if (name.length() == 0 || lastName.length() == 0) {
            return false;
        }
        Student student = new Student(name, lastName);
        student.setGroupId(groupId);
        return studentDao.insert(student);
    }

    public boolean deleteStudent(int id) {
        if (id < 1 ) {
            return false;
        }
        return studentDao.deleteById(id);
    }

    public boolean addStudentToCourse(int studentId, int courseId) {
        if (studentId < 1 || courseId < 1) {
            return false;
        }
        return studentDao.addToCourse(studentId, courseId);
    }

    public boolean removeStudentFromCourse(int studentId, int courseId) {
        if (studentId < 1 || courseId < 1) {
            return false;
        }
        return studentDao.deleteFromCourse(studentId, courseId);
    }
}
