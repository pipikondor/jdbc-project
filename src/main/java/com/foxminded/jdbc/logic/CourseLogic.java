package com.foxminded.jdbc.logic;

import com.foxminded.jdbc.dao.interfaces.CourseDao;
import com.foxminded.jdbc.model.Student;

import java.util.ArrayList;
import java.util.List;

public class CourseLogic {

    private final CourseDao courseDao;

    public CourseLogic(CourseDao courseDao) {
        this.courseDao = courseDao;
    }

    public List<Student> findStudentsByCourse(String courseName) {
        if (courseName == null) {
            return new ArrayList<>();
        }
        return courseDao.findStudents(courseName);
    }
}
