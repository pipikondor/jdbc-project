package com.foxminded.jdbc.logic;

import com.foxminded.jdbc.dao.interfaces.CourseDao;
import com.foxminded.jdbc.dao.interfaces.GroupDao;
import com.foxminded.jdbc.dao.interfaces.StudentDao;
import com.foxminded.jdbc.model.Course;
import com.foxminded.jdbc.model.Group;
import com.foxminded.jdbc.model.Student;

import java.util.List;
import java.util.stream.Collectors;

public class SchoolDataFiller {

    private final DataGenerator dataGenerator;
    private final StudentDao studentDao;
    private final GroupDao groupDao;
    private final CourseDao courseDao;
    private final ResourceLoader resourceLoader;

    public SchoolDataFiller(DataGenerator dataGenerator, StudentDao studentDao, GroupDao groupDao,
                            CourseDao courseDao, ResourceLoader resourceLoader) {
        this.dataGenerator = dataGenerator;
        this.studentDao = studentDao;
        this.groupDao = groupDao;
        this.courseDao = courseDao;
        this.resourceLoader = resourceLoader;
    }

    public boolean fillDatabase() {
        if (!fillCourses()) {
            return false;
        }
        if (!fillGroups()) {
            return false;
        }
        if (!fillStudents(setGroupToStudents(createStudents()))) {
            return false;
        }
        return fillStudentsCourses(setCoursesToStudent());
    }

    private List<Student> setCoursesToStudent() {
        int maxCourseCount = Integer.parseInt(resourceLoader.getPropertyValue("maxCourseCount"));
        List<Student> studentsWithId = studentDao.findAll();
        List<Course> courses = courseDao.findAll();
        return dataGenerator.assignCoursesToStudents(studentsWithId, courses, maxCourseCount);
    }

    private List<Student> createStudents() {
        List<String> names = resourceLoader.getFileAsStream("names").collect(Collectors.toList());
        List<String> lastNames = resourceLoader.getFileAsStream("lastNames").collect(Collectors.toList());
        int studentsCount = Integer.parseInt(resourceLoader.getPropertyValue("studentsCount"));
        return dataGenerator.generateStudents(names, lastNames, studentsCount);
    }

    private List<Student> setGroupToStudents(List<Student> students) {
        int groupMaxSize = Integer.parseInt(resourceLoader.getPropertyValue("groupMaxSize"));
        int groupMinSize = Integer.parseInt(resourceLoader.getPropertyValue("groupMinSize"));
        List<Group> groups = groupDao.findAll();
        return dataGenerator.assignGroupsToStudents(students, groups, groupMaxSize, groupMinSize);
    }

    private boolean fillStudents(List<Student> students) {
        for (Student student : students) {
            if (!studentDao.insert(student)) {
                return false;
            }
        }
        return true;
    }

    private boolean fillCourses() {
        List<String> coursesNames = resourceLoader.getFileAsStream("courses").collect(Collectors.toList());
        List<Course> courses = coursesNames.stream().map(Course::new).collect(Collectors.toList());
        for (Course course : courses) {
            if (!courseDao.insert(course)) {
                return false;
            }
        }
        return true;
    }

    private boolean fillGroups() {
        int groupCount = Integer.parseInt(resourceLoader.getPropertyValue("groupCount"));
        List<Group> groups = dataGenerator.generateGroups(groupCount);
        for (Group group : groups) {
            if (!groupDao.insert(group)) {
                return false;
            }
        }
        return true;
    }

    private boolean fillStudentsCourses(List<Student> students) {
        for (Student student: students) {
            for (Integer courseId: student.getCourses()) {
                if (!studentDao.addToCourse(student.getId(), courseId)) {
                    return false;
                }
            }
        }
        return true;
    }
}
