package com.foxminded.jdbc.logic;

import com.foxminded.jdbc.model.Course;
import com.foxminded.jdbc.model.Group;
import com.foxminded.jdbc.model.Student;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DataGenerator {

    public static final Random RANDOM = new Random();

    public List<Group> generateGroups(int count) {
        List<Group> groups = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            String groupName = generateGroupName();
            groups.add(new Group(groupName));
        }
        return groups;
    }

    private String generateGroupName() {
        char hyphen = '-';
        StringBuilder groupName = new StringBuilder();
        groupName.append((char) (RANDOM.nextInt(26) + 'a'));
        groupName.append((char) (RANDOM.nextInt(26) + 'a'));
        groupName.append(hyphen);
        groupName.append(RANDOM.nextInt(90) + 10);
        return groupName.toString();
    }

    public List<Student> generateStudents(List<String> names, List<String> lastNames, int count) {
        List<Student> students = new ArrayList<>();
        if (names == null || lastNames == null) {
            return students;
        }
        if (names.isEmpty() || lastNames.isEmpty()) {
            return students;
        }
        for (int i = 0; i < count; i++) {
            String randomName = names.get(RANDOM.nextInt(names.size()));
            String randomLastName = lastNames.get(RANDOM.nextInt(lastNames.size()));
            students.add(new Student(randomName, randomLastName));
        }
        return students;
    }

    public List<Student> assignGroupsToStudents(List<Student> students, List<Group> groups, int maxGroupSize, int minGroupSize) {
        if (students == null) {
            return new ArrayList<>();
        }
        if (groups == null  || groups.isEmpty()) {
            return students;
        }
        if (maxGroupSize < minGroupSize || maxGroupSize < 1 || minGroupSize < 0) {
            throw new IllegalArgumentException();
        }
        students.forEach(student -> {
            Group randomGroup = groups.get(DataGenerator.RANDOM.nextInt(groups.size()));
            if (randomGroup.getStudentsCount() < maxGroupSize) {
                student.setGroupId(randomGroup.getId());
                randomGroup.setStudentsCount(randomGroup.getStudentsCount() + 1);
            }
        });
        return cleanGroupsLessMinSize(students, groups, minGroupSize);
    }

    private List<Student> cleanGroupsLessMinSize(List<Student> students, List<Group> groups, int minGroupSize) {
        List<Integer> emptyGroupsId = new ArrayList<>();
        groups.stream().filter(group -> group.getStudentsCount() < minGroupSize).forEach(group -> {
            group.setStudentsCount(0);
            emptyGroupsId.add(group.getId());
        });
        students.stream().filter(student -> student.getGroupId() != 0)
            .filter(student -> emptyGroupsId.contains(student.getGroupId())).forEach(student -> student.setGroupId(0));
        return students;
    }

    public List<Student> assignCoursesToStudents(List<Student> students, List<Course> courses, int maxCourseCount) {
        if (students == null) {
            return new ArrayList<>();
        }
        if (courses == null || courses.isEmpty()) {
            return students;
        }
        if (maxCourseCount > courses.size()) {
            maxCourseCount = courses.size();
        }
        for (Student student: students) {
            int[] randomCourses = DataGenerator.RANDOM.ints(0, courses.size())
                .distinct().limit(DataGenerator.RANDOM.nextInt(maxCourseCount) + 1L)
                .map(index -> courses.get(index).getId()).toArray();
            student.setCourses(randomCourses);
        }
        return students;
    }
}
