package com.foxminded.jdbc;

import com.foxminded.jdbc.dao.ScriptExecutor;
import com.foxminded.jdbc.dao.interfaces.CourseDao;
import com.foxminded.jdbc.dao.interfaces.GroupDao;
import com.foxminded.jdbc.dao.interfaces.StudentDao;
import com.foxminded.jdbc.dao.impl.CourseDaoImpl;
import com.foxminded.jdbc.dao.impl.GroupDaoImpl;
import com.foxminded.jdbc.dao.impl.StudentDaoImpl;
import com.foxminded.jdbc.logic.ResourceLoader;
import com.foxminded.jdbc.logic.SchoolDataFiller;
import com.foxminded.jdbc.logic.DataGenerator;
import com.foxminded.jdbc.logic.CourseLogic;
import com.foxminded.jdbc.logic.StudentLogic;
import com.foxminded.jdbc.logic.GroupLogic;
import com.foxminded.jdbc.menu.Menu;

import java.util.stream.Collectors;

public class Main {

    private static final String ERROR_MESSAGE = "Something went wrong";

    public static void main(String[] args) {
        ResourceLoader resourceLoader = new ResourceLoader();
        DataGenerator dataGenerator = new DataGenerator();
        StudentDao studentDao = new StudentDaoImpl();
        CourseDao courseDao = new CourseDaoImpl();
        GroupDao groupDao = new GroupDaoImpl();
        StudentLogic studentLogic = new StudentLogic(studentDao);
        CourseLogic courseLogic = new CourseLogic(courseDao);
        GroupLogic groupLogic = new GroupLogic(groupDao);
        String sqlScript = resourceLoader.getFileAsStream("sqlCreateTables").collect(Collectors.joining(" "));
        if (!(new ScriptExecutor().createEmptyTables(sqlScript))) {
            System.out.println(ERROR_MESSAGE);
            return;
        }
        SchoolDataFiller schoolDataFiller = new SchoolDataFiller(dataGenerator, studentDao, groupDao, courseDao, resourceLoader);
        if (!schoolDataFiller.fillDatabase()) {
            System.out.println(ERROR_MESSAGE);
            return;
        }
        Menu menu = new Menu(courseLogic, studentLogic, groupLogic, resourceLoader);
        menu.runApp();
    }
}
