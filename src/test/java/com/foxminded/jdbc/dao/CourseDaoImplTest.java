package com.foxminded.jdbc.dao;

import com.foxminded.jdbc.dao.impl.CourseDaoImpl;
import com.foxminded.jdbc.logic.ResourceLoader;
import com.foxminded.jdbc.model.Course;
import com.foxminded.jdbc.model.Student;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

class CourseDaoImplTest {

    private static CourseDaoImpl courseDao;

    @BeforeAll
    static void createMemoryDatabase() throws FileNotFoundException {
        ResourceLoader resourceLoader = new ResourceLoader();
        String sqlScriptFileName = resourceLoader.getPropertyValue("sqlCreateTestTables");
        try (Connection connection = Connect.getInstance().getConnection()) {
            InputStream sqlCreateStream = CourseDaoImplTest.class.getClassLoader().getResourceAsStream(sqlScriptFileName);
            if (sqlCreateStream == null) {
                throw new FileNotFoundException();
            }
            Reader reader = new InputStreamReader(sqlCreateStream);
            ScriptRunner scriptRunner = new ScriptRunner(connection);
            scriptRunner.setLogWriter(null);
            scriptRunner.runScript(reader);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        courseDao = new CourseDaoImpl();
    }

    @Test
    void insert_shouldInsertCourseToTable_whenInputIsNotNull() throws SQLException {
        Connection connection = Connect.getInstance().getConnection();
        connection.setAutoCommit(false);
        Course exceptedCourse = new Course("Law");
        courseDao.insert(exceptedCourse);
        Course actualCourse = courseDao.findAll().get(2);
        Assertions.assertEquals(exceptedCourse.getName(), actualCourse.getName());
        connection.rollback();
    }

    @Test
    void insert_shouldReturnTrue_whenInputIsNotNull() throws SQLException {
        Connection connection = Connect.getInstance().getConnection();
        connection.setAutoCommit(false);
        Course course = new Course("Law");
        Assertions.assertTrue(courseDao.insert(course));
        connection.rollback();
    }

    @Test
    void insert_shouldNotChangeTable_whenInputIsNull() throws SQLException {
        Connection connection = Connect.getInstance().getConnection();
        connection.setAutoCommit(false);
        int exceptedSize = courseDao.findAll().size();
        courseDao.insert(null);
        int actualSize = courseDao.findAll().size();
        Assertions.assertEquals(exceptedSize, actualSize);
        connection.rollback();
    }

    @Test
    void insert_shouldReturnFalse_whenInputIsNull() throws SQLException {
        Connection connection = Connect.getInstance().getConnection();
        connection.setAutoCommit(false);
        Assertions.assertFalse(courseDao.insert(null));
        connection.rollback();
    }

    @Test
    void findStudents_shouldReturnEmptyList_whenInputIsNull() {
        List<Student> students = new ArrayList<>();
        Assertions.assertEquals(students, courseDao.findStudents(null));
    }

    @Test
    void findStudents_shouldReturnTwoStudents_whenCourseIsFinance() {
        List<Student> students = new ArrayList<>();
        Student student = new Student("Alan", "Twon");
        student.setId(1);
        students.add(student);
        student = new Student("Jack", "Brat");
        student.setId(3);
        student.setGroupId(1);
        students.add(student);
        Assertions.assertEquals(students, courseDao.findStudents("Finance"));
    }

    @Test
    void findStudents_shouldReturnEmptyList_whenCourseIsNotFound() {
        List<Student> students = new ArrayList<>();
        Assertions.assertEquals(students, courseDao.findStudents("abc"));
    }

    @Test
    void findStudents_shouldReturnEmptyList_whenCourseIsEmpty() {
        List<Student> students = new ArrayList<>();
        Assertions.assertEquals(students, courseDao.findStudents("IT"));
    }

    @Test
    void findAll_shouldReturnAllGroups_whenTableHasNotChanges() {
        List<Course> courses = new ArrayList<>();
        Course course = new Course("Finance");
        course.setId(1);
        courses.add(course);
        course = new Course("IT");
        course.setId(2);
        courses.add(course);
        Assertions.assertEquals(courses, courseDao.findAll());
    }
}
