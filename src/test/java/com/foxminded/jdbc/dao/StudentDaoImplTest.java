package com.foxminded.jdbc.dao;

import com.foxminded.jdbc.dao.impl.CourseDaoImpl;
import com.foxminded.jdbc.dao.impl.StudentDaoImpl;
import com.foxminded.jdbc.logic.ResourceLoader;
import com.foxminded.jdbc.model.Student;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

class StudentDaoImplTest {

    private static StudentDaoImpl studentDao;

    @BeforeAll
    static void createMemoryDatabase() throws FileNotFoundException {
        ResourceLoader resourceLoader = new ResourceLoader();
        String sqlScriptFileName = resourceLoader.getPropertyValue("sqlCreateTestTables");
        try (Connection connection = Connect.getInstance().getConnection()) {
            InputStream sqlCreateStream = StudentDaoImplTest.class.getClassLoader().getResourceAsStream(sqlScriptFileName);
            if (sqlCreateStream == null) {
                throw new FileNotFoundException();
            }
            Reader reader = new InputStreamReader(sqlCreateStream);
            ScriptRunner scriptRunner = new ScriptRunner(connection);
            scriptRunner.setLogWriter(null);
            scriptRunner.runScript(reader);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        studentDao = new StudentDaoImpl();
    }

    @Test
    void insert_shouldReturnFalse_whenInputIsNull() throws SQLException {
        Connection connection = Connect.getInstance().getConnection();
        connection.setAutoCommit(false);
        Assertions.assertFalse(studentDao.insert(null));
        connection.rollback();
    }

    @Test
    void insert_shouldNotChangeTable_whenInputIsNull() throws SQLException {
        Connection connection = Connect.getInstance().getConnection();
        connection.setAutoCommit(false);
        int exceptedTableSize = studentDao.findAll().size();
        studentDao.insert(null);
        int actualTableSize = studentDao.findAll().size();
        Assertions.assertEquals(exceptedTableSize, actualTableSize);
        connection.rollback();
    }

    @Test
    void insert_shouldInsertStudentInTable_whenInputIsNotNull() throws SQLException {
        Connection connection = Connect.getInstance().getConnection();
        connection.setAutoCommit(false);
        Student student = new Student("Nick", "Neil");
        student.setGroupId(1);
        studentDao.insert(student);
        Student actualStudent = studentDao.findAll().get(4);
        student.setId(5);
        Assertions.assertEquals(student, actualStudent);
        connection.rollback();
    }

    @Test
    void insert_shouldReturnTrue_whenInputIsNotNull() throws SQLException {
        Connection connection = Connect.getInstance().getConnection();
        connection.setAutoCommit(false);
        Student student = new Student("Nick", "Neil");
        student.setGroupId(1);
        Assertions.assertTrue(studentDao.insert(student));
        connection.rollback();
    }

    @Test
    void insert_shouldInsertNumericNullIntoGroupId_whenGroupIdIsZero() throws SQLException {
        Connection connection = Connect.getInstance().getConnection();
        connection.setAutoCommit(false);
        Student student = new Student("Nick", "Neil");
        student.setId(0);
        studentDao.insert(student);
        Student actualStudent = studentDao.findAll().get(4);
        Assertions.assertEquals(0, actualStudent.getGroupId());
        connection.rollback();
    }

    @Test
    void findAll_shouldReturnAllStudents_whenTableHasNotChanges() {
        List<Student> students = new ArrayList<>();
        Student student = new Student("Alan", "Twon");
        student.setId(1);
        students.add(student);
        student = new Student("Alex", "Flor");
        student.setId(2);
        student.setGroupId(1);
        students.add(student);
        student = new Student("Jack", "Brat");
        student.setId(3);
        student.setGroupId(1);
        students.add(student);
        student = new Student("Soon", "Rancho");
        student.setId(4);
        student.setGroupId(2);
        students.add(student);
        Assertions.assertEquals(students, studentDao.findAll());
    }

    @Test
    void addToCourse_shouldReturnFalse_whenStudentIdIsNotFound() throws SQLException {
        Connection connection = Connect.getInstance().getConnection();
        connection.setAutoCommit(false);
        Assertions.assertFalse(studentDao.addToCourse(10, 1));
        connection.rollback();
    }

    @Test
    void addToCourse_shouldReturnFalse_whenCourseIsNotFound() throws SQLException {
        Connection connection = Connect.getInstance().getConnection();
        connection.setAutoCommit(false);
        Assertions.assertFalse(studentDao.addToCourse(1, 11));
        connection.rollback();
    }

    @Test
    void addToCourse_shouldReturnTrue_whenStudentIdAndCourseIdIsExist() throws SQLException {
        Connection connection = Connect.getInstance().getConnection();
        connection.setAutoCommit(false);
        Assertions.assertTrue(studentDao.addToCourse(1, 2));
        connection.rollback();
    }

    @Test
    void addToCourse_shouldReturnFalse_whenStudentIdAlreadyHasInputCourse() throws SQLException {
        Connection connection = Connect.getInstance().getConnection();
        connection.setAutoCommit(false);
        Assertions.assertFalse(studentDao.addToCourse(1, 1));
        connection.rollback();
    }

    @Test
    void addToCourse_shouldAddStudentToCourse_whenStudentIdAndCourseIdIsExist() throws SQLException {
        Connection connection = Connect.getInstance().getConnection();
        connection.setAutoCommit(false);
        studentDao.addToCourse(1, 2);
        Student student = new Student("Alan", "Twon");
        student.setId(1);
        CourseDaoImpl courseDao = new CourseDaoImpl();
        Student exceptedStudent = courseDao.findStudents("IT").get(0);
        Assertions.assertEquals(student, exceptedStudent);
        connection.rollback();
    }

    @Test
    void deleteById_shouldReturnFalse_whenIdIsNotFound() throws SQLException {
        Connection connection = Connect.getInstance().getConnection();
        connection.setAutoCommit(false);
        Assertions.assertFalse(studentDao.deleteById(5));
        connection.rollback();
    }

    @Test
    void deleteById_shouldReturnTrue_whenStudentWithIdIsExist() throws SQLException {
        Connection connection = Connect.getInstance().getConnection();
        connection.setAutoCommit(false);
        Assertions.assertTrue(studentDao.deleteById(1));
        connection.rollback();
    }

    @Test
    void deleteById_shouldDeleteStudent_whenStudentWithIdIsExist() throws SQLException {
        Connection connection = Connect.getInstance().getConnection();
        connection.setAutoCommit(false);
        int exceptedTableSize = studentDao.findAll().size() - 1;
        Assertions.assertTrue(studentDao.deleteById(1));
        int actualTableSize = studentDao.findAll().size();
        Assertions.assertEquals(exceptedTableSize, actualTableSize);
        connection.rollback();
    }

    @Test
    void deleteFromCourse_shouldReturnFalse_whenStudentHasNotCourse() throws SQLException {
        Connection connection = Connect.getInstance().getConnection();
        connection.setAutoCommit(false);
        Assertions.assertFalse(studentDao.deleteFromCourse(1, 6));
        connection.rollback();
    }

    @Test
    void deleteFromCourse_shouldReturnTrue_whenDeletionIsSuccess() throws SQLException {
        Connection connection = Connect.getInstance().getConnection();
        connection.setAutoCommit(false);
        Assertions.assertTrue(studentDao.deleteFromCourse(3, 1));
        connection.rollback();
    }

    @Test
    void deleteFromCourse_shouldDeleteStudentFromCourse_whenStudentIdAndCourseIdIsExist() throws SQLException {
        Connection connection = Connect.getInstance().getConnection();
        connection.setAutoCommit(false);
        CourseDaoImpl courseDao = new CourseDaoImpl();
        int exceptedTableSize = courseDao.findStudents("Finance").size() - 1;
        studentDao.deleteFromCourse(3, 1);
        int actualTableSize = courseDao.findStudents("Finance").size();
        Assertions.assertEquals(exceptedTableSize, actualTableSize);
        connection.rollback();
    }
}
