package com.foxminded.jdbc.dao;

import com.foxminded.jdbc.dao.impl.GroupDaoImpl;
import com.foxminded.jdbc.logic.ResourceLoader;
import com.foxminded.jdbc.model.Group;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.InputStream;
import java.io.FileNotFoundException;
import java.io.Reader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

class GroupDaoImplTest {

    private static GroupDaoImpl groupDao;

    @BeforeAll
    static void createMemoryDatabase() throws FileNotFoundException {
        ResourceLoader resourceLoader = new ResourceLoader();
        String sqlScriptFileName = resourceLoader.getPropertyValue("sqlCreateTestTables");
        try (Connection connection = Connect.getInstance().getConnection()) {
            InputStream sqlCreateStream = GroupDaoImpl.class.getClassLoader().getResourceAsStream(sqlScriptFileName);
            if (sqlCreateStream == null) {
                throw new FileNotFoundException();
            }
            Reader reader = new InputStreamReader(sqlCreateStream);
            ScriptRunner scriptRunner = new ScriptRunner(connection);
            scriptRunner.setLogWriter(null);
            scriptRunner.runScript(reader);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        groupDao = new GroupDaoImpl();
    }

    @Test
    void insert_shouldInsertGroupInTable_whenInputIsNotNull() throws SQLException {
        Connection connection = Connect.getInstance().getConnection();
        connection.setAutoCommit(false);
        String groupName = "fx-64";
        Group group = new Group(groupName);
        groupDao.insert(group);
        List<Group> groups = groupDao.findAll();
        String actualName = groups.get(3).getName();
        Assertions.assertEquals(groupName, actualName);
        connection.rollback();
    }

    @Test
    void insert_shouldReturnTrue_whenInputIsNotNull() throws SQLException {
        Connection connection = Connect.getInstance().getConnection();
        connection.setAutoCommit(false);
        String groupName = "fx-64";
        Group group = new Group(groupName);
        Assertions.assertTrue(groupDao.insert(group));
        connection.rollback();
    }

    @Test
    void insert_shouldReturnFalse_whenInputIsNull() throws SQLException {
        Connection connection = Connect.getInstance().getConnection();
        connection.setAutoCommit(false);
        Assertions.assertFalse(groupDao.insert(null));
        connection.rollback();
    }

    @Test
    void insert_shouldNotChangeDb_whenInputIsNull() throws SQLException {
        Connection connection = Connect.getInstance().getConnection();
        connection.setAutoCommit(false);
        int exceptedTableSize = groupDao.findAll().size();
        groupDao.insert(null);
        int actualTableSize =groupDao.findAll().size();
        Assertions.assertEquals(exceptedTableSize, actualTableSize);
        connection.rollback();
    }

    @Test
    void findAll_shouldReturnAllGroups_whenTableHasNotChanges() {
        List<Group> groups = new ArrayList<>();
        Group group = new Group("ab-37");
        group.setId(1);
        groups.add(group);
        group = new Group("zg-90");
        group.setId(2);
        groups.add(group);
        group = new Group("ft-82");
        group.setId(3);
        groups.add(group);
        Assertions.assertEquals(groups, groupDao.findAll());
    }

    @Test
    void findAllLessSize_shouldReturnOneGroupInList_whenCountEqualsZero() {
        List<Group> groups = new ArrayList<>();
        Group group = new Group("ft-82");
        group.setId(3);
        groups.add(group);
        Assertions.assertEquals(groups, groupDao.findAllLessSize(0));
    }

    @Test
    void findAllLessSize_shouldReturnAllGroups_whenCountEqualsTwo() {
        List<Group> groups = new ArrayList<>();
        Group group = new Group("ab-37");
        group.setId(1);
        groups.add(group);
        group = new Group("zg-90");
        group.setId(2);
        groups.add(group);
        group = new Group("ft-82");
        group.setId(3);
        groups.add(group);
        Assertions.assertEquals(groups, groupDao.findAllLessSize(2));
    }

    @Test
    void findAllLessSize_shouldReturnAllGroups_whenCountLessZero() {
        List<Group> groups = new ArrayList<>();
        Assertions.assertEquals(groups, groupDao.findAllLessSize(-5));
    }
}
