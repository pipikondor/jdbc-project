package com.foxminded.jdbc.logic;

import com.foxminded.jdbc.dao.interfaces.GroupDao;
import com.foxminded.jdbc.dao.impl.GroupDaoImpl;
import com.foxminded.jdbc.model.Group;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyInt;

class GroupLogicTest {

    private GroupDao groupDaoMock;
    private GroupLogic groupLogic;

    @BeforeEach
    void createGroupLogic() {
        groupDaoMock = Mockito.mock(GroupDaoImpl.class);
        groupLogic = new GroupLogic(groupDaoMock);
    }

    @Test
    void findGroupsByLength_shouldReturnEmptyList_whenInputLessZero() {
        List<Group> groups = new ArrayList<>();
        Assertions.assertEquals(groups, groupLogic.findGroupsByLength(-5));
        Mockito.verify(groupDaoMock, Mockito.times(0)).findAllLessSize(anyInt());
    }

    @Test
    void findGroupsByLength_shouldReturnGroupsList_whenInputHasGroupLength() {
        List<Group> groups = new ArrayList<>();
        groups.add(new Group("ft-56"));
        groups.add(new Group("zs-52"));
        Mockito.when(groupDaoMock.findAllLessSize(anyInt())).thenReturn(groups);
        Assertions.assertEquals(groups, groupLogic.findGroupsByLength(5));
        Mockito.verify(groupDaoMock, Mockito.times(1)).findAllLessSize(anyInt());
    }
}
