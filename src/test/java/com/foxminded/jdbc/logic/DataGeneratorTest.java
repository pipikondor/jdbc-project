package com.foxminded.jdbc.logic;

import com.foxminded.jdbc.model.Course;
import com.foxminded.jdbc.model.Group;
import com.foxminded.jdbc.model.Student;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.ArrayList;
import java.util.List;

class DataGeneratorTest {

    private DataGenerator dataGenerator;

    @BeforeEach
    void createDataGenerator() {
        dataGenerator = new DataGenerator();
    }

    @Test
    void generateStudents_shouldReturnEmptyList_whenNamesIsNull() {
        List<String> names = new ArrayList<>();
        names.add("Edward");
        List<Student> exceptedList = new ArrayList<>();
        Assertions.assertEquals(exceptedList, dataGenerator.generateStudents(names, null, 10));
    }

    @Test
    void generateStudents_shouldReturnEmptyList_whenLastNamesIsNull() {
        List<String> lastNames = new ArrayList<>();
        lastNames.add("Twon");
        List<Student> exceptedList = new ArrayList<>();
        Assertions.assertEquals(exceptedList, dataGenerator.generateStudents(null, lastNames, 10));
    }

    @Test
    void generateStudents_shouldReturnEmptyList_whenLastNamesAndNamesIsNull() {
        List<Student> exceptedList = new ArrayList<>();
        Assertions.assertEquals(exceptedList, dataGenerator.generateStudents(null, null, 10));
    }

    @Test
    void generateStudents_shouldReturnEmptyList_whenNamesIsEmpty() {
        List<String> names = new ArrayList<>();
        List<String> lastNames = new ArrayList<>();
        lastNames.add("Twon");
        List<Student> exceptedList = new ArrayList<>();
        Assertions.assertEquals(exceptedList, dataGenerator.generateStudents(names, lastNames, 10));
    }

    @Test
    void generateStudents_shouldReturnEmptyList_whenLastNamesIsEmpty() {
        List<String> names = new ArrayList<>();
        List<String> lastNames = new ArrayList<>();
        names.add("Edward");
        List<Student> exceptedList = new ArrayList<>();
        Assertions.assertEquals(exceptedList, dataGenerator.generateStudents(names, lastNames, 10));
    }

    @Test
    void generateStudents_shouldReturnEmptyList_whenNamesAndLastNamesIsEmpty() {
        List<String> names = new ArrayList<>();
        List<String> lastNames = new ArrayList<>();
        List<Student> exceptedList = new ArrayList<>();
        Assertions.assertEquals(exceptedList, dataGenerator.generateStudents(names, lastNames, 10));
    }

    @ParameterizedTest
    @ValueSource(ints = {-5, 0})
    void generateStudents_shouldReturnEmptyList_whenCountEqualsOrLessZero(int count) {
        List<String> names = new ArrayList<>();
        names.add("Edward");
        List<String> lastNames = new ArrayList<>();
        lastNames.add("Twon");
        List<Student> exceptedList = new ArrayList<>();
        Assertions.assertEquals(exceptedList, dataGenerator.generateStudents(names, lastNames, count));
    }

    @Test
    void generateStudents_shouldReturnTwoStudents_whenCountIsTwoAndInputListsIsNotEmpty() {
        List<String> names = new ArrayList<>();
        names.add("Edward");
        List<String> lastNames = new ArrayList<>();
        lastNames.add("Twon");
        List<Student> exceptedList = new ArrayList<>();
        exceptedList.add(new Student("Edward", "Twon"));
        exceptedList.add(new Student("Edward", "Twon"));
        Assertions.assertEquals(exceptedList, dataGenerator.generateStudents(names, lastNames, 2));
    }

    @ParameterizedTest
    @ValueSource(ints = {-5, 0})
    void generateGroups_shouldReturnEmptyList_whenCountEqualsOrLessZero(int count) {
        List<Group> exceptedList = new ArrayList<>();
        Assertions.assertEquals(exceptedList, dataGenerator.generateGroups(count));
    }

    @Test
    void generateGroups_shouldReturnGroupNamesWhichMatchPattern_whenCountIsOne() {
        String groupName = dataGenerator.generateGroups(1).get(0).getName();
        Assertions.assertTrue(groupName.matches("[a-z]{2}-[0-9]{2}"));
    }

    @Test
    void assignCoursesToStudents_shouldReturnEmptyList_whenStudentsIsNull() {
        List<Student> students = new ArrayList<>();
        List<Course> courses = new ArrayList<>();
        Course course = new Course("Law");
        course.setId(1);
        courses.add(course);
        Assertions.assertEquals(students, dataGenerator.assignCoursesToStudents(null, courses, 3));
    }

    @Test
    void assignCoursesToStudents_shouldReturnInputStudents_whenCoursesIsNull() {
        List<Student> students = new ArrayList<>();
        Student student = new Student("Alan", "Twon");
        students.add(student);
        Assertions.assertEquals(students, dataGenerator.assignCoursesToStudents(students, null, 3));
    }

    @Test
    void assignCoursesToStudents_shouldReturnStudentsWithCourseId_whenCoursesSizeLessMaxCourseCount() {
        List<Student> students = new ArrayList<>();
        Student student = new Student("Alan", "Twon");
        students.add(student);
        List<Course> courses = new ArrayList<>();
        Course course = new Course("Law");
        course.setId(1);
        courses.add(course);
        Student exceptedStudent = new Student("Alan", "Twon");
        exceptedStudent.setCourses(new int []{1});
        List<Student> actualStudents = dataGenerator.assignCoursesToStudents(students, courses, 3);
        Assertions.assertEquals(exceptedStudent, actualStudents.get(0));
    }

    @Test
    void assignGroupsToStudents_shouldReturnInputStudents_whenGroupsIsNull() {
        List<Student> students = new ArrayList<>();
        Student student = new Student("Alina", "Port");
        students.add(student);
        Assertions.assertEquals(students, dataGenerator.assignGroupsToStudents(students, null, 10, 5));
    }

    @Test
    void assignGroupsToStudents_shouldReturnEmptyList_whenStudentsIsNull() {
        List<Student> students = new ArrayList<>();
        List<Group> groups = new ArrayList<>();
        Group group = new Group("sd-26");
        groups.add(group);
        Assertions.assertEquals(students, dataGenerator.assignGroupsToStudents(null, groups, 10, 5));
    }

    @Test
    void assignGroupsToStudents_shouldThrowIllegalArgumentException_whenMaxGroupSizeLessOne() {
        List<Student> students = new ArrayList<>();
        List<Group> groups = new ArrayList<>();
        Group group = new Group("df-56");
        group.setId(1);
        groups.add(group);
        Assertions.assertThrows(IllegalArgumentException.class, () -> dataGenerator.assignGroupsToStudents(students, groups, 0, 1));
    }

    @Test
    void assignGroupsToStudents_shouldThrowIllegalArgumentException_whenMinGroupSizeLessZero() {
        List<Student> students = new ArrayList<>();
        List<Group> groups = new ArrayList<>();
        Group group = new Group("df-56");
        group.setId(1);
        groups.add(group);
        Assertions.assertThrows(IllegalArgumentException.class, () -> dataGenerator.assignGroupsToStudents(students, groups, 1, -4));
    }

    @Test
    void assignGroupsToStudents_shouldThrowIllegalArgumentException_whenMaxGroupSizeLessMinGroupSize() {
        List<Student> students = new ArrayList<>();
        List<Group> groups = new ArrayList<>();
        Group group = new Group("df-56");
        group.setId(1);
        groups.add(group);
        Assertions.assertThrows(IllegalArgumentException.class, () -> dataGenerator.assignGroupsToStudents(students, groups, 1, 3));
    }

    @Test
    void assignGroupsToStudents_shouldReturnStudentsWithoutGroups_whenStudentsInGroupLessMinSize() {
        List<Student> students = new ArrayList<>();
        students.add(new Student("Alan", "Twon"));
        List<Group> groups = new ArrayList<>();
        Group group = new Group("df-56");
        group.setId(1);
        groups.add(group);
        List<Student> exceptedStudents = new ArrayList<>();
        exceptedStudents.add(new Student("Alan", "Twon"));
        Assertions.assertEquals(exceptedStudents, dataGenerator.assignGroupsToStudents(students, groups, 10, 3));
    }

    @Test
    void assignGroupsToStudents_shouldReturnStudentsWithGroups_whenStudentsInGroupGreaterMaxSize() {
        List<Student> students = new ArrayList<>();
        students.add(new Student("Alan", "Twon"));
        students.add(new Student("Alac", "Twos"));
        students.add(new Student("Dlan", "Ewon"));
        students.add(new Student("Alach", "Twon"));
        students.add(new Student("Dlan", "Twony"));
        students.add(new Student("Apan", "Ewone"));
        students.add(new Student("Alan", "Ewonq"));
        students.add(new Student("Alan", "Twos"));
        students.add(new Student("Alsan", "Twon"));
        students.add(new Student("Alan", "Twone"));
        List<Group> groups = new ArrayList<>();
        Group group = new Group("df-56");
        group.setId(1);
        groups.add(group);
        List<Student> exceptedStudents = new ArrayList<>();
        exceptedStudents.add(new Student("Alan", "Twon"));
        exceptedStudents.add(new Student("Alac", "Twos"));
        exceptedStudents.add(new Student("Dlan", "Ewon"));
        exceptedStudents.add(new Student("Alach", "Twon"));
        exceptedStudents.add(new Student("Dlan", "Twony"));
        exceptedStudents.add(new Student("Apan", "Ewone"));
        exceptedStudents.add(new Student("Alan", "Ewonq"));
        exceptedStudents.add(new Student("Alan", "Twos"));
        exceptedStudents.add(new Student("Alsan", "Twon"));
        exceptedStudents.add(new Student("Alan", "Twone"));
        exceptedStudents.forEach(student -> student.setGroupId(1));
        Assertions.assertEquals(exceptedStudents, dataGenerator.assignGroupsToStudents(students, groups, 10, 9));
    }
}
