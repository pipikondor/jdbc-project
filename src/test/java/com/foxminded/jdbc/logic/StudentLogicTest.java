package com.foxminded.jdbc.logic;

import com.foxminded.jdbc.dao.interfaces.StudentDao;
import com.foxminded.jdbc.dao.impl.StudentDaoImpl;
import com.foxminded.jdbc.model.Student;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;

class StudentLogicTest {

    private StudentLogic studentLogic;
    private StudentDao studentDaoMock;

    @BeforeEach
    void createStudentLogic() {
        studentDaoMock = Mockito.mock(StudentDaoImpl.class);
        studentLogic = new StudentLogic(studentDaoMock);
    }

    @Test
    void addStudent_shouldReturnFalse_whenNameIsNull() {
        Assertions.assertFalse(studentLogic.addStudent(null, "lastname", 1));
        Mockito.verify(studentDaoMock, Mockito.times(0)).insert(any(Student.class));
    }

    @Test
    void addStudent_shouldReturnFalse_whenLastNameIsNull() {
        Assertions.assertFalse(studentLogic.addStudent("name", null, 1));
        Mockito.verify(studentDaoMock, Mockito.times(0)).insert(any(Student.class));
    }

    @Test
    void addStudent_shouldReturnFalse_whenGroupIdLessZero() {
        Assertions.assertFalse(studentLogic.addStudent("name", "lastname", -5));
        Mockito.verify(studentDaoMock, Mockito.times(0)).insert(any(Student.class));
    }

    @Test
    void addStudent_shouldReturnFalse_whenNameIsEmptyString() {
        Assertions.assertFalse(studentLogic.addStudent("", "lastname", 5));
        Mockito.verify(studentDaoMock, Mockito.times(0)).insert(any(Student.class));
    }

    @Test
    void addStudent_shouldReturnFalse_whenLastNameIsEmptyString() {
        Assertions.assertFalse(studentLogic.addStudent("name", "", 5));
        Mockito.verify(studentDaoMock, Mockito.times(0)).insert(any(Student.class));
    }

    @Test
    void addStudent_shouldReturnTrue_whenStudentWasAdded() {
        Mockito.when(studentDaoMock.insert(any(Student.class))).thenReturn(true);
        Assertions.assertTrue(studentLogic.addStudent("name", "lastname", 5));
        Mockito.verify(studentDaoMock, Mockito.times(1)).insert(any(Student.class));
    }

    @Test
    void addStudent_shouldReturnFalse_whenStudentWasNotAdded() {
        Mockito.when(studentDaoMock.insert(any(Student.class))).thenReturn(false);
        Assertions.assertFalse(studentLogic.addStudent("name", "lastname", 5));
        Mockito.verify(studentDaoMock, Mockito.times(1)).insert(any(Student.class));
    }

    @Test
    void deleteStudent_shouldReturnFalse_whenStudentIdLessOne() {
        Assertions.assertFalse(studentLogic.deleteStudent(0));
        Mockito.verify(studentDaoMock, Mockito.times(0)).deleteById(anyInt());
    }

    @Test
    void deleteStudent_shouldReturnTrue_whenStudentWasDeleted() {
        Mockito.when(studentDaoMock.deleteById(anyInt())).thenReturn(true);
        Assertions.assertTrue(studentLogic.deleteStudent(3));
        Mockito.verify(studentDaoMock, Mockito.times(1)).deleteById(anyInt());
    }

    @Test
    void deleteStudent_shouldReturnFalse_whenStudentWasNotDeleted() {
        Mockito.when(studentDaoMock.deleteById(anyInt())).thenReturn(false);
        Assertions.assertFalse(studentLogic.deleteStudent(2));
        Mockito.verify(studentDaoMock, Mockito.times(1)).deleteById(anyInt());
    }

    @Test
    void addStudentToCourse_shouldReturnFalse_whenStudentIdLessOne() {
        Assertions.assertFalse(studentLogic.addStudentToCourse(0, 5));
        Mockito.verify(studentDaoMock, Mockito.times(0)).addToCourse(anyInt(), anyInt());
    }

    @Test
    void addStudentToCourse_shouldReturnTrue_whenStudentWasAddedToCourse() {
        Mockito.when(studentDaoMock.addToCourse(anyInt(), anyInt())).thenReturn(true);
        Assertions.assertTrue(studentLogic.addStudentToCourse(5, 4));
        Mockito.verify(studentDaoMock, Mockito.times(1)).addToCourse(anyInt(), anyInt());
    }

    @Test
    void addStudentToCourse_shouldReturnFalse_whenStudentWasNotAddedToCourse() {
        Mockito.when(studentDaoMock.addToCourse(anyInt(), anyInt())).thenReturn(false);
        Assertions.assertFalse(studentLogic.addStudentToCourse(5, 3));
        Mockito.verify(studentDaoMock, Mockito.times(1)).addToCourse(anyInt(), anyInt());
    }

    @Test
    void removeStudentFromCourse_shouldReturnFalse_whenCourseIdLessOne() {
        Assertions.assertFalse(studentLogic.removeStudentFromCourse(5, -5));
        Mockito.verify(studentDaoMock, Mockito.times(0)).deleteFromCourse(anyInt(), anyInt());
    }

    @Test
    void removeStudentFromCourse_shouldReturnFalse_whenStudentWasNotDeletedFromCourse() {
        Mockito.when(studentDaoMock.deleteFromCourse(anyInt(), anyInt())).thenReturn(false);
        Assertions.assertFalse(studentLogic.removeStudentFromCourse(4, 5));
        Mockito.verify(studentDaoMock, Mockito.times(1)).deleteFromCourse(anyInt(), anyInt());
    }

    @Test
    void removeStudentFromCourse_shouldReturnTrue_whenStudentWasDeletedFromCourse() {
        Mockito.when(studentDaoMock.deleteFromCourse(anyInt(), anyInt())).thenReturn(true);
        Assertions.assertTrue(studentLogic.removeStudentFromCourse(3, 5));
        Mockito.verify(studentDaoMock, Mockito.times(1)).deleteFromCourse(anyInt(), anyInt());
    }
}
