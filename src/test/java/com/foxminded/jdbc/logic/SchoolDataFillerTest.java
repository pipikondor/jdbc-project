package com.foxminded.jdbc.logic;

import com.foxminded.jdbc.dao.interfaces.CourseDao;
import com.foxminded.jdbc.dao.interfaces.GroupDao;
import com.foxminded.jdbc.dao.interfaces.StudentDao;
import com.foxminded.jdbc.dao.impl.CourseDaoImpl;
import com.foxminded.jdbc.dao.impl.GroupDaoImpl;
import com.foxminded.jdbc.dao.impl.StudentDaoImpl;
import com.foxminded.jdbc.model.Course;
import com.foxminded.jdbc.model.Group;
import com.foxminded.jdbc.model.Student;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.any;

class SchoolDataFillerTest {

    private SchoolDataFiller schoolDataFiller;
    private StudentDao studentDaoMock;
    private CourseDao courseDaoMock;
    private GroupDao groupDaoMock;
    private ResourceLoader resourceLoaderMock;
    private DataGenerator dataGeneratorMock;

    @BeforeEach
    void createSchoolDataFillerDependenciesObjects() {
        studentDaoMock = Mockito.mock(StudentDaoImpl.class);
        courseDaoMock = Mockito.mock(CourseDaoImpl.class);
        groupDaoMock = Mockito.mock(GroupDaoImpl.class);
        dataGeneratorMock = Mockito.mock(DataGenerator.class);
        resourceLoaderMock = Mockito.mock(ResourceLoader.class);
        schoolDataFiller = new SchoolDataFiller(dataGeneratorMock, studentDaoMock, groupDaoMock, courseDaoMock, resourceLoaderMock);
    }

    @Test
    void fillDatabase_shouldReturnFalse_whenFillCoursesReturnFalse() {
        Mockito.when(resourceLoaderMock.getFileAsStream("courses")).thenReturn(Stream.of("Law"));
        Mockito.when(courseDaoMock.insert(any(Course.class))).thenReturn(false);
        Assertions.assertFalse(schoolDataFiller.fillDatabase());
        Mockito.verify(groupDaoMock, Mockito.times(0)).insert(any(Group.class));
    }

    @Test
    void fillDatabase_shouldReturnFalse_whenFillGroupsReturnFalse() {
        List<Group> groups = new ArrayList<>();
        Group group = new Group("fs-92");
        groups.add(group);
        Mockito.when(resourceLoaderMock.getPropertyValue("groupCount")).thenReturn("1");
        Mockito.when(dataGeneratorMock.generateGroups(anyInt())).thenReturn(groups);
        Mockito.when(courseDaoMock.insert(any(Course.class))).thenReturn(true);
        Mockito.when(groupDaoMock.insert(any(Group.class))).thenReturn(false);
        Assertions.assertFalse(schoolDataFiller.fillDatabase());
        Mockito.verify(studentDaoMock, Mockito.times(0)).insert(any(Student.class));
    }

    @Test
    void fillDatabase_shouldReturnFalse_whenFillStudentsReturnFalse() {
        List<Student> students = new ArrayList<>();
        Student student = new Student("Alan", "Twon");
        student.setCourses(new int[] {1});
        students.add(student);
        Mockito.when(resourceLoaderMock.getPropertyValue("groupCount")).thenReturn("1");
        Mockito.when(resourceLoaderMock.getPropertyValue("studentsCount")).thenReturn("1");
        Mockito.when(resourceLoaderMock.getPropertyValue("groupMaxSize")).thenReturn("3");
        Mockito.when(resourceLoaderMock.getPropertyValue("groupMinSize")).thenReturn("1");
        Mockito.when(dataGeneratorMock.assignGroupsToStudents(anyList(), anyList(), anyInt(), anyInt())).thenReturn(students);
        Mockito.when(courseDaoMock.insert(any(Course.class))).thenReturn(true);
        Mockito.when(groupDaoMock.insert(any(Group.class))).thenReturn(true);
        Mockito.when(studentDaoMock.insert(any(Student.class))).thenReturn(false);
        Assertions.assertFalse(schoolDataFiller.fillDatabase());
        Mockito.verify(studentDaoMock, Mockito.times(0)).addToCourse(anyInt(), anyInt());
    }

    @Test
    void fillDatabase_shouldReturnFalse_whenFillStudentsCoursesReturnFalse() {
        List<Student> students = new ArrayList<>();
        Student student = new Student("Alan", "Twon");
        student.setCourses(new int[] {1});
        students.add(student);
        Mockito.when(resourceLoaderMock.getPropertyValue("groupCount")).thenReturn("1");
        Mockito.when(resourceLoaderMock.getPropertyValue("studentsCount")).thenReturn("1");
        Mockito.when(resourceLoaderMock.getPropertyValue("groupMaxSize")).thenReturn("3");
        Mockito.when(resourceLoaderMock.getPropertyValue("groupMinSize")).thenReturn("1");
        Mockito.when(resourceLoaderMock.getPropertyValue("maxCourseCount")).thenReturn("1");
        Mockito.when(dataGeneratorMock.assignCoursesToStudents(anyList(), anyList(), anyInt())).thenReturn(students);
        Mockito.when(courseDaoMock.insert(any(Course.class))).thenReturn(true);
        Mockito.when(groupDaoMock.insert(any(Group.class))).thenReturn(true);
        Mockito.when(studentDaoMock.insert(any(Student.class))).thenReturn(true);
        Mockito.when(studentDaoMock.addToCourse(anyInt(), anyInt())).thenReturn(false);
        Assertions.assertFalse(schoolDataFiller.fillDatabase());
        Mockito.verify(studentDaoMock, Mockito.times(1)).addToCourse(anyInt(), anyInt());
    }

    @Test
    void fillDatabase_shouldReturnTrue_whenAllTablesWasFilled() {
        List<Student> students = new ArrayList<>();
        Student student = new Student("Alan", "Twon");
        student.setCourses(new int[] {1});
        students.add(student);
        Mockito.when(resourceLoaderMock.getPropertyValue("groupCount")).thenReturn("1");
        Mockito.when(resourceLoaderMock.getPropertyValue("studentsCount")).thenReturn("1");
        Mockito.when(resourceLoaderMock.getPropertyValue("groupMaxSize")).thenReturn("3");
        Mockito.when(resourceLoaderMock.getPropertyValue("groupMinSize")).thenReturn("1");
        Mockito.when(resourceLoaderMock.getPropertyValue("maxCourseCount")).thenReturn("1");
        Mockito.when(dataGeneratorMock.assignCoursesToStudents(anyList(), anyList(), anyInt())).thenReturn(students);
        Mockito.when(courseDaoMock.insert(any(Course.class))).thenReturn(true);
        Mockito.when(groupDaoMock.insert(any(Group.class))).thenReturn(true);
        Mockito.when(studentDaoMock.insert(any(Student.class))).thenReturn(true);
        Mockito.when(studentDaoMock.addToCourse(anyInt(), anyInt())).thenReturn(true);
        Assertions.assertTrue(schoolDataFiller.fillDatabase());
    }
}
