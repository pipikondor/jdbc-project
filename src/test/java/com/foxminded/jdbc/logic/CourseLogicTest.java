package com.foxminded.jdbc.logic;

import com.foxminded.jdbc.dao.interfaces.CourseDao;
import com.foxminded.jdbc.dao.impl.CourseDaoImpl;
import com.foxminded.jdbc.model.Student;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;

class CourseLogicTest {

    private CourseLogic courseLogic;
    private CourseDao courseDaoMock;

    @BeforeEach
    void createCourseLogic() {
        courseDaoMock = Mockito.mock(CourseDaoImpl.class);
        courseLogic = new CourseLogic(courseDaoMock);
    }

    @Test
    void findStudentsByCourse_shouldReturnStudentsList_whenCourseHasStudents() {
        List<Student> students = new ArrayList<>();
        Student student = new Student("Alan", "Twon");
        students.add(student);
        Mockito.when(courseDaoMock.findStudents(anyString())).thenReturn(students);
        Assertions.assertEquals(students, courseLogic.findStudentsByCourse("abc"));
        Mockito.verify(courseDaoMock, Mockito.times(1)).findStudents(anyString());
    }

    @Test
    void findStudentsByCourse_shouldReturnEmptyList_whenCourseIsNull() {
        List<Student> students = new ArrayList<>();
        Assertions.assertEquals(students, courseLogic.findStudentsByCourse(null));
        Mockito.verify(courseDaoMock, Mockito.times(0)).findStudents(anyString());
    }
}
