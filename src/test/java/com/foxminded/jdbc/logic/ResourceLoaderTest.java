package com.foxminded.jdbc.logic;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

class ResourceLoaderTest {

    private ResourceLoader resourceLoader;

    @BeforeEach
    void createResourceLoaderObject() {
        resourceLoader = new ResourceLoader();
    }

    @Test
    void getPropertyValue_shouldReturnEmptyString_whenPropertyNameIsNull() {
        Assertions.assertEquals("", resourceLoader.getPropertyValue(null));
    }

    @Test
    void getPropertyValue_shouldReturnEmptyString_whenPropertyValueIsNull(){
        Assertions.assertEquals("", resourceLoader.getPropertyValue("emptyPropertyValue"));
    }

    @Test
    void getPropertyValue_shouldReturnEmptyString_whenPropertyNameIsNotExist(){
        Assertions.assertEquals("", resourceLoader.getPropertyValue("abc"));
    }

    @Test
    void getPropertyValue_shouldReturnPropertyValueAsTest_whenPropertyNameIsExist(){
        Assertions.assertEquals("Test", resourceLoader.getPropertyValue("testProperty"));
    }

    @Test
    void getFileAsStream_shouldReturnEmptyStringInStream_whenPropertyNameIsNotExist(){
        List<String> exceptedList = new ArrayList<>();
        exceptedList.add("");
        Assertions.assertEquals(exceptedList, resourceLoader.getFileAsStream("abc").collect(Collectors.toList()));
    }

    @Test
    void getFileAsStream_shouldReturnEmptyStringInStream_whenPropertyNameIsNull(){
        List<String> exceptedList = new ArrayList<>();
        exceptedList.add("");
        Assertions.assertEquals(exceptedList, resourceLoader.getFileAsStream(null).collect(Collectors.toList()));
    }

    @Test
    void getFileAsStream_shouldReturnFileAsStream_whenPropertyNameAndPropertyValueIsExist(){
        List<String> exceptedList = new ArrayList<>();
        exceptedList.add("Law");
        exceptedList.add("course");
        Assertions.assertEquals(exceptedList, resourceLoader.getFileAsStream("courses").collect(Collectors.toList()));
    }
}
