DROP TABLE IF EXISTS groups;

CREATE TABLE groups (
    group_id INT generated by default as identity(start with 1),
    group_name character varying (50) NOT NULL,
    PRIMARY KEY (group_id)
);

DROP TABLE IF EXISTS students CASCADE;

CREATE TABLE students(
    student_id INT generated by default as identity(start with 1),
    group_id INT,
    first_name character varying(50) NOT NULL,
    last_name character varying(50) NOT NULL,
    PRIMARY KEY (student_id)
);

DROP TABLE IF EXISTS courses CASCADE;

CREATE TABLE courses(
    course_id INT generated by default as identity(start with 1),
    course_name character varying(50) NOT NULL,
    course_description VARCHAR(200),
    PRIMARY KEY (course_id)
);

DROP TABLE IF EXISTS students_courses;

CREATE TABLE students_courses(
    student_id INT NOT NULL,
    course_id INT NOT NULL,
    FOREIGN KEY (student_id) REFERENCES students (student_id) ON DELETE CASCADE,
    FOREIGN KEY (course_id) REFERENCES courses (course_id) ON DELETE CASCADE,
    PRIMARY KEY (student_id, course_id)
);

INSERT INTO students(first_name, last_name) VALUES('Alan', 'Twon');
INSERT INTO students(group_id, first_name, last_name) VALUES(1, 'Alex', 'Flor');
INSERT INTO students(group_id, first_name, last_name) VALUES(1, 'Jack', 'Brat');
INSERT INTO students(group_id, first_name, last_name) VALUES(2, 'Soon', 'Rancho');
INSERT INTO groups(group_name) VALUES('ab-37');
INSERT INTO groups(group_name) VALUES('zg-90');
INSERT INTO groups(group_name) VALUES('ft-82');
INSERT INTO courses(course_name) VALUES('Finance');
INSERT INTO courses(course_name) VALUES('IT');
INSERT INTO students_courses(student_id, course_id) VALUES (1, 1);
INSERT INTO students_courses(student_id, course_id) VALUES (3, 1);